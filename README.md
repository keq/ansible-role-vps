# VPS Role
A role for a Debian 11 VPS to install and setup the very basics.
First run init to create the suoder user for the rest of the role.
```bash
./init.yml -i root@12.34.45.67, -k
./run.yml -i admin@12.34.45.67,
```
