set fish_greeting
set fish_color_normal ffffff
set fish_color_command 61afef
set fish_color_quote e5c07b
set fish_color_redirection c678dd
set fish_color_end c678dd
set fish_color_error f2777a
set fish_color_param 98c379
set fish_color_selection white --bold --background=brblack
set fish_color_search_match bryellow --background=black
set fish_color_history_current --bold
set fish_color_operator 00a6b2
set fish_color_escape 00a6b2
set fish_color_cwd 98c379
set fish_color_cwd_root f2777a
set fish_color_valid_path --underline
set fish_color_autosuggestion d3dae3
set fish_color_user 98c379
set fish_color_host normal
set fish_color_cancel -r
set fish_pager_color_completion e5c07b
set fish_pager_color_description e5c07b
set fish_pager_color_prefix 66cccc --underline
set fish_pager_color_progress brwhite --background=cyan
set fish_color_comment 586e75
set fish_color_match --background=brblue

function n --description 'support nnn quit and change directory'
    # Block nesting of nnn in subshells
    if test -n NNNLVL
        if [ (expr $NNNLVL + 0) -ge 1 ]
            echo "nnn is already running"
            return
        end
    end

    # The default behaviour is to cd on quit (nnn checks if NNN_TMPFILE is set)
    # To cd on quit only on ^G, remove the "-x" as in:
    #    set NNN_TMPFILE "$XDG_CONFIG_HOME/nnn/.lastd"
    # NOTE: NNN_TMPFILE is fixed, should not be modified
    if test -n "$XDG_CONFIG_HOME"
        set NNN_TMPFILE "$XDG_CONFIG_HOME/nnn/.lastd"
    else
        set NNN_TMPFILE "$HOME/.config/nnn/.lastd"
    end

    # Unmask ^Q (, ^V etc.) (if required, see `stty -a`) to Quit nnn
    # stty start undef
    # stty stop undef
    # stty lwrap undef
    # stty lnext undef

    nnn -C $argv

    if test -e $NNN_TMPFILE
        source $NNN_TMPFILE
        rm $NNN_TMPFILE
    end
end

set -x NNN_COLORS '4215'

function __p_p
  function fish_prompt
    echo (pwd)"> "
  end
end
function p
  fish --private -C __p_p
end
